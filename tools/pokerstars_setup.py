import os
import sys
import shutil
from PIL import Image

def edit_pokerstars_image(f, alpha):

	im = Image.open(f)
        im = Image.new("RGBA", im.size, (0, 0, 0, alpha))
        im.save(f)

editable = [['/Gx/chips&deck/chips/0/', 0],
	    ['/Gx/chips&deck/chips/1/', 0],
	    ['/Gx/chips&deck/chips/2/', 0],
	    ['/Gx/chips&deck/chips/3/', 0],
	    ['/Gx/chips&deck/chips/4/', 0],
	    ['/Gx/chips&deck/chips/5/', 0],
	    ['/Gx/chips&deck/chips/6/', 0],
	    ['/Themes/nova/green/chips&deck/chips/0/', 0],
	    ['/Themes/nova/green/chips&deck/chips/1/', 0],
	    ['/Themes/nova/green/chips&deck/chips/2/', 0],
	    ['/Themes/nova/green/chips&deck/chips/3/', 0],
	    ['/Themes/nova/green/chips&deck/chips/4/', 0],
	    ['/Themes/nova/green/chips&deck/chips/5/', 0],
	    ['/Themes/nova/green/chips&deck/chips/6/', 0],
	    ['/Themes/nova/green/label/', 255],
	    ['/Themes/nova/green/', 255]]

pokerstars_path = "C:\Program Files (x86)\PokerStars.RO"
if len(sys.argv) > 1:
    pokerstars_path = sys.argv[1]

for e in editable:
	for f in os.listdir(pokerstars_path + e[0]):
		if f[-4:] not in ['.png', '.bmp']:
			continue
		edit_pokerstars_image(pokerstars_path + e[0] + f, e[1])

os.system('py -2.7 ./ini_encrypter.py "{}\Themes\nova\green/"'.format(pokerstars_path))
shutil.copyfile('./gx.ini', '{}/Themes/nova/green/gx.ini'.format(pokerstars_path))
os.system('py -2.7 ./ini_encrypter.py "{}\Themes\nova\green/"'.format(pokerstars_path))
