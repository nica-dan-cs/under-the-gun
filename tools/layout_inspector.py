from observation import observer as observer_m
from observation import poker_table as poker_table_m
from PIL import Image
from PIL import ImageOps
import sys, tty, termios
import time

import contextlib
with contextlib.redirect_stdout(None):
	import pygame

class layout_inspector:

	def __init__(self, poker_table_location, layout_name):

		self.observer = observer_m.observer(poker_table_location, layout_name = layout_name)
		self.poker_table = self.observer.poker_table
		self.layout = self.poker_table.layout
		self.poker_table.refresh()

		pygame.init()
		self.pygame_display = pygame.display.set_mode(self.poker_table.dimensions)

		self.uie = []
		for k in [ k for k in self.layout.keys() if k != 'table']:
			for i in range(len(self.layout[k])):
				self.uie.append((k,i))
		self.i = 0

	def getch(self):

		fd = sys.stdin.fileno()
		old_settings = termios.tcgetattr(fd)
		try:
			tty.setraw(sys.stdin.fileno())
			ch = sys.stdin.read(1)
		finally:
			termios.tcsetattr(fd, termios.TCSADRAIN, old_settings)
		return ch

	def poker_table_image(self):

		image = Image.blend(self.poker_table.ui_element('table', 0),
				    Image.new("RGB", self.poker_table.dimensions, 'black'), 0.7)

		for i in range(len(self.uie)):

			uie_image = self.poker_table.ui_element(self.uie[i][0], self.uie[i][1])
			if (i == self.i):
				uie_image = uie_image.crop((1, 1, uie_image.size[0] - 1, uie_image.size[1] - 1))
				uie_image = ImageOps.expand(uie_image, border = 1, fill = 'white')

			image.paste(uie_image,
				    (int(self.layout[self.uie[i][0]][self.uie[i][1]][0] * self.poker_table.dimensions[0]),
				     int(self.layout[self.uie[i][0]][self.uie[i][1]][1] * self.poker_table.dimensions[1])))

		return image

	def process_command(self, command):

		if command == 'q':
			print(self.layout)
			exit(0)

		elif command in ['o','O']:
			self.pygame_display = pygame.display.set_mode((1,1), pygame.NOFRAME)
			self.observer.observe_table()
			self.pygame_display = pygame.display.set_mode(self.poker_table.dimensions)

		elif command == 'r':
			self.pygame_display = pygame.display.set_mode((1,1), pygame.NOFRAME)
			self.poker_table.refresh()
			self.pygame_display = pygame.display.set_mode(self.poker_table.dimensions)

		elif command == '.':
			self.i += 1
			if self.i == len(self.uie):
				self.i = 0
		elif command == ',':
			self.i -= 1
			if self.i < 0:
				self.i = len(self.uie) - 1

		k = self.uie[self.i][0]
		i = self.uie[self.i][1]

		if command == 'w':
			self.layout[k][i][1] -= 0.001
		elif command == 'a':
			self.layout[k][i][0] -= 0.001
		elif command == 'd':
			self.layout[k][i][0] += 0.001
		elif command == 's':
			self.layout[k][i][1] += 0.001


		elif command == 'W':
			self.layout[k][i][3] -= 0.001
		elif command == 'A':
			self.layout[k][i][2] -= 0.001
		elif command == 'D':
			self.layout[k][i][2] += 0.001
		elif command == 'S':
			self.layout[k][i][3] += 0.001

	def run(self):

		while True:

			image = self.poker_table_image()
			pygame_image = pygame.image.fromstring(image.tobytes(),
							       image.size,
							       image.mode)

			self.pygame_display.blit(pygame_image, (0,0))
			pygame.display.flip()
			self.process_command(self.getch())
