import itertools

ranks = '23456789TJQKA'
suits = 'dhcs'
deck = [ r + s for r in ranks for s in suits]
all_possible_combinations = [i for i in itertools.combinations(deck,2)]

def all_combos(hand):

	for i in all_possible_combinations:
		if (i[0][0] == hand[0] and i[1][0] == hand[1]) or (i[0][0] == hand[1] and i[1][0] == hand[0]):
			yield i

def all_better_combos(cards):

	if cards[0] == cards[1]:
		betterpairs = [i+i for i in ranks if ranks.index(i) >= ranks.index(cards[0])]
		for x in betterpairs:
			for hand in all_combos(x):
				yield hand
	else:
		bettercards = [cards[0]+i for i in ranks if ranks.index(i) >= ranks.index(cards[1]) and ranks.index(i) < ranks.index(cards[0])]
		for x in bettercards:
			for hand in all_combos(x):
				yield hand

def all_combos_in(hands):
	handrange = []
	for hand in hands:
		if hand[-1] != '+':
			for hand in all_combos(hand):
				handrange.append(hand)
		else:
			for hand in all_better_combos(hand):
				handrange.append(hand)
	return handrange
