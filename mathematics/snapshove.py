import requests
import getpass

class snapshove:

	def __init__(self):

		self.session = requests.Session()
		self.session.post('https://www.snapshove.com/login',
				  {'email': input('(Snapshove) Email:'),
				   'password': getpass.getpass('(Snapshove) Password:')})

	def calculate_shove(self, ante, bigblinds, players, position):

		data = {'action': 'Shove',
			'ante': str(ante),
			'bigblind': str(bigblinds),
			'player': str(players),
			'position': str(position)}

		print('\n')
		print(data)
		r = self.session.post('https://www.snapshove.com/calculateshove', data)
		return r.json()['ranges'].split(',')

	def calculate_call(self, ante, bigblinds, players, shove_position, call_position):

		data = {'action': 'Call',
			'ante': str(ante),
			'bigblind': str(bigblinds),
			'player': str(players),
			'shovePosition': str(shove_position),
			'callPosition': str(call_position)}

		print('\n')
		print(data)
		r = self.session.post('https://www.snapshove.com/calculatecall', data)
		return r.json()['hero']['ranges'].split(',')

