import sys
from mathematics import snapshove as snapshove_m
from observation import observer as observer_m
from thinking import thinker as thinker_m
from acting import actor as actor_m

location = [int(sys.argv[i]) for i in range(1,5)]
layout = str(sys.argv[5])

snapshove = snapshove_m.snapshove()
observer = observer_m.observer(location, layout_name = layout)
thinker = thinker_m.thinker(snapshove)
actor = actor_m.actor()

while 1 == 1:

	observer.poker_table.refresh()
	if observer.observe_buttons() == ['','','']:
		continue

	table = observer.observe_table()
	dillema = thinker.interpret(table)
	decision = thinker.think(dillema)
	actor.act(decision)

	print('\nDecision: {}\n'.format(decision))
	input('Press any key to continue...')
