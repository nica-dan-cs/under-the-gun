from mathematics import ranges

class thinker:

	def __init__(self, snapshove):

		self.snapshove = snapshove

	def to_literal_position(self, players, delta_utg):

		return ['LJ', 'HJ', 'CO', 'BTN', 'SB', 'BB'][-players:][delta_utg]

	def verbose_dillema(self, d):

		print('\nAnte: {}\nHero: H:{} S:{} - UTG+{}'.format(d['ante'],
								    d['hero-hand'],
								    d['hero-blinds'],
								    d['hero-pos']))
		print('OOP:')
		for i in range(len(d['v-oop'])):
			print('    S:{0:.2f} O:{1}'.format(d['v-oop'][i]['blinds'],
						     d['v-oop'][i]['ontable']))
		print('IP:')
		for i in range(len(d['v-ip'])):
			print('    S:{0:.2f} O:{1}'.format(d['v-ip'][i]['blinds'],
						     d['v-ip'][i]['ontable']))

	def interpret(self, table):

		ontables_nonzero = []
		for ontable in table['ontables']:
			if ontable > 0:
				ontables_nonzero.append(ontable)

		small_blind = min(ontables_nonzero)
		big_blind =  2 * small_blind

		d = {}
		d['hero-hand'] = (table['hero-hand'][:2], table['hero-hand'][2:])
		d['hero-blinds'] = int(table['stacks'][0] / big_blind)

		for i in range(len(table['ontables'])):
			if table['ontables'][i] == small_blind:
				small_blind_i = i
				break
		utg_i = small_blind_i + 2
		if utg_i > len(table['ontables']) - 1:
			utg_i = utg_i - len(table['ontables'])

		d['v-oop'] = []
		first_oop_i = utg_i
		if first_oop_i == 0:
                        first_oop_i = 1
		for i in range(first_oop_i, len(table['ontables'])):
			if not table['folded'][i]:
				d['v-oop'].append({})
				d['v-oop'][-1]['blinds'] = float(table['stacks'][i] / big_blind)
				d['v-oop'][-1]['ontable'] = float(table['ontables'][i] / big_blind)

		d['v-ip'] = []
		last_ip_i = utg_i
		if last_ip_i == 0:
			last_ip_i = len(table['ontables'])
		for i in range(1, last_ip_i):
			if not table['folded'][i]:
				d['v-ip'].append({})
				d['v-ip'][-1]['blinds'] = float(table['stacks'][i] / big_blind)
				d['v-ip'][-1]['ontable'] = float(table['ontables'][i] / big_blind)

		d['ante'] = 10
		d['players'] = int(len(d['v-oop']) + len(d['v-ip']) + 1)
		d['hero-pos'] = len(d['v-oop'])

		self.verbose_dillema(d)
		return d

	def decide(self, hand, snapshove_range):

		if hand in ranges.all_combos_in(snapshove_range):
			return 'Push'
		else:
			return 'Fold'

	def think(self, d):

		ante = d['ante']
		bigblinds = d['hero-blinds']
		players = d['players']
		position = str(self.to_literal_position(players, d['hero-pos']))
		hero_hand = d['hero-hand']

		if bigblinds > 25:
			return self.decide(hero_hand, 'QQ+,AKo+,AKs+')

		if len(d['v-oop']) == 0:

			shove_ranges = self.snapshove.calculate_shove(ante, bigblinds, players, position)
			return self.decide(hero_hand, shove_ranges)

		if sum(1 for villain in d['v-oop'] if villain['ontable'] > 1) == 0:

			tightest_position = self.to_literal_position(players, 0)
			shove_ranges = self.snapshove.calculate_shove(ante, bigblinds, players, tightest_position)
			return self.decide(hero_hand, shove_ranges)

		if sum(1 for villain in d['v-oop'] if villain['ontable'] > 1) == 1:

			raiser_real_stack = d['v-oop'][-1]['blinds'] + d['v-oop'][-1]['ontable']
			mes = min([25, d['hero-blinds'], raiser_real_stack])

			ses_stacks = [villain['blinds'] for villain in d['v-ip']]
			ses_stacks.append(25)
			ses_stacks.append(d['hero-blinds'])
			ses = min(ses_stacks)

			if mes <= ses/5:
				shove_ranges = self.snapshove.calculate_shove(ante, bigblinds, players, position)
				return self.decide(hero_hand, shove_ranges)
			else:
				villain_position = str(self.to_literal_position(players, d['hero-pos'] -1))
				call_ranges = self.snapshove.calculate_call(ante, bigblinds, players, villain_position, position)
				return self.decide(hero_hand, call_ranges)

		if sum(1 for villain in d['v-oop'] if villain['ontable'] > 1) > 1:

			villain_position = str(self.to_literal_position(players, 0))
			position = str(self.to_literal_position(players, 1))
			call_ranges = self.snapshove.calculate_call(ante, bigblinds, players, villain_position, position)
			return self.decide(hero_hand, call_ranges)

		assert(False)
