import pyautogui

from observation import poker_table_layouts

class poker_table:

	def __init__(self, location, layout_name = 'pokerstars-6max'):

		self.location = location
		self.dimensions = (self.location[2] - self.location[0],
				   self.location[3] - self.location[1])
		self.layout = poker_table_layouts.layouts[layout_name]

	def refresh(self):

		self.image = pyautogui.screenshot(region = self.location)

	def to_absolute_location(self, relative_location):

		absolute_location = [0,0,0,0]
		absolute_location[0] = int(relative_location[0] * self.dimensions[0])
		absolute_location[1] = int(relative_location[1] * self.dimensions[1])
		absolute_location[2] = int(relative_location[2] * self.dimensions[0])
		absolute_location[3] = int(relative_location[3] * self.dimensions[1])
		return absolute_location

	def ui_element(self, ui_element_name, index):

		absolute_location = self.to_absolute_location(self.layout[ui_element_name][index])
		return self.image.crop(absolute_location)

