import sys
import time
import subprocess
from PIL import Image
from observation import poker_table as poker_table_m

class observer:

	def __init__(self, poker_table_location, layout_name = 'pokerstars-6max'):

		self.poker_table = poker_table_m.poker_table(poker_table_location, layout_name)

	def verbose_table(self, table):

		print('\nHero: {}'.format(table['hero-hand']))
		print('Pot: {}'.format(table['pot']))
		for i in range(len(table['folded'])):

			if table['folded'][i]:
				has_folded = '- has folded'
			else:
				has_folded = ''

			print('({}): S:{} O:{} {}'.format(i,
							  table['stacks'][i],
							  table['ontables'][i],
							  has_folded))

		print('Buttons: {}'.format(table['buttons']))
		print('Time Elapsed: {}'.format(table['time-elapsed']))

	def cutout_image_background(self, image):

		tl = [0, 0]
		br = [image.size[0], image.size[1]]
		l = image.load()

		should_break = False
		for x in range(image.size[0]):
			for y in range(image.size[1]):
				if l[x,y][1] > 25:
					should_break = True
					break
			if should_break:
				break
			else:
				tl[0] = tl[0] + 1

		should_break = False
		for x in range(image.size[0]):
			for y in range(image.size[1]):
				if l[image.size[0] - 1 - x,y][1] > 25:
					should_break = True
					break
			if should_break:
				break
			else:
				br[0] = br[0] - 1

		should_break = False
		for y in range(image.size[1]):
			for x in range(image.size[0]):
				if l[x,y][1] > 25:
					should_break = True
					break
			if should_break:
				break
			else:
				tl[1] = tl[1] + 1

		should_break = False
		for y in range(image.size[1]):
			for x in range(image.size[0]):
				if l[x, image.size[1] - 1 - y][1] > 25:
					should_break = True
					break
			if should_break:
				break
			else:
				br[1] = br[1] - 1

		if tl[0] > br[0]:
			return Image.new("RGB", (1, 1), (0, 0, 0))
		else:
			return image.crop((tl[0], tl[1], br[0], br[1]))

	def call_tesseract(self, image, psm, alphabet):

		tesseract_filename = 'input.{}'.format('bmp')
		tesseract_psm = '-psm {}'.format(psm)
		tesseract_alphabet = 'tessedit_char_whitelist=\'{}\''.format(alphabet)

		image.save(tesseract_filename)
		tesseract = subprocess.check_output(['tesseract', tesseract_filename, 'stdout',
					    '-psm', psm, '-c', tesseract_alphabet])

		return tesseract.decode('UTF-8').replace('\n','').replace('\r','')

	def observe_pot(self):

		im = self.poker_table.ui_element('pot', 0)
		pot = self.call_tesseract(im, '7', 'Pot: $0123456789,')
		pot = pot.replace(',','')
		return int(pot.split(' ')[1][1:])

	def observe_stacks(self):

		stacks = [ 0 for i in range(len(self.poker_table.layout['stacks'])) ]

		for i in range(len(stacks)):

			im = self.poker_table.ui_element('stacks', i)
			im = self.cutout_image_background(im)

			if im.size[0] == 1:
				stacks[i] = int(0)
				continue

			stack = self.call_tesseract(im, '8', '$01234567890,')
			stack = stack.replace(',','')
			stacks[i] = str(stack)[1:]

			if not stacks[i].isdigit():
				stacks[i] = int(0)
			else:
				stacks[i] = int(stacks[i])

		return stacks

	def observe_ontables(self):

		ontables = [ 0 for i in range(len(self.poker_table.layout['ontables'])) ]

		for i in range(len(ontables)):

			im = self.poker_table.ui_element('ontables', i)
			im = self.cutout_image_background(im)

			if im.size[0] == 1:
				ontables[i] = int(0)
				continue
			else:
				ontable = self.call_tesseract(im, '8', '$01234567890,')
				ontable = ontable.replace(',','')

			if not ontable == '':
				try:
					ontables[i] = int(str(ontable)[1:])
				except:
					ontables[i] = int(0)
			else:
				ontables[i] = int(0)

		return ontables

	def observe_hero_hand(self):

		hero_hand = ''

		for i in range(2):
			im = self.poker_table.ui_element('hero-hand', i)
			rank = self.call_tesseract(im, '10', '1234567890JQKAT')
			if rank == '':
				rank = 'T'

			r,g,b,n = 0,0,0,0
			l = im.load()
			for x in range(3, im.size[0]):
				for y in range(3,im.size[1]):
					if l[x,y] <= (240,240,240):
						r += l[x,y][0]
						g += l[x,y][1]
						b += l[x,y][2]
						n += 1
			r /= n
			g /= n
			b /= n

			suit = 'd'
			if r < 140 and g < 140 and b < 140:
				suit = 's'
			elif r > g and r > b:
				suit = 'h'
			elif g > r and g > b:
				suit = 'c'

			hero_hand = hero_hand + rank + suit

		return hero_hand

	def observe_folded(self):

		folded = [True for i in range(len(self.poker_table.layout['folded']))]
		folded[0] = False

		for i in range(1, len(folded)):
			im = self.poker_table.ui_element('folded', i)
			all_pixels = im.load()
			for x in range(im.size[0]):
				for y in range(im.size[1]):
					if all_pixels[x,y][0] > 190:
						folded[i] = False
						break
				if folded[i] == False:
					break

		return folded

	def observe_buttons(self):

		buttons = ['Fold', 'Check/Call', 'Bet/Raise']

		for i in range(3):
			im = self.poker_table.ui_element('buttons', i)
			l = im.load()
			r = 0
			for x in range(im.size[0]):
				for y in range(im.size[1]):
					r += l[x,y][0]

			if r / (im.size[0] * im.size[1]) < 55:
				buttons[i] = ''

		return buttons

	def observe_table(self):

		table = {}
		timestamp_begin = time.time()

		self.poker_table.refresh()
		folded = self.observe_folded()
		table['hero-hand'] = self.observe_hero_hand()
		table['pot'] = self.observe_pot()
		table['folded'] = folded
		table['stacks'] = self.observe_stacks()
		table['ontables'] = self.observe_ontables()
		table['buttons'] = self.observe_buttons()

		timestamp_end = time.time()
		table['time-elapsed'] = timestamp_end - timestamp_begin

		self.verbose_table(table)
		return table
